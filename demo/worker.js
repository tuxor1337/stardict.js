/**
 * Copyright (c) 2024 Thomas Vogt
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import { StarDictSync } from "../dist/stardict.js";

onmessage = (evt) => {
  let dict = new StarDictSync(evt.data);
  let name = dict.keyword("bookname");

  let synonyms = [];
  let synonymsRaw = dict.synonyms({
    include_offset: true,
    include_wid: false,
    include_term: false,
  });
  if (synonymsRaw.length > 0) {
    synonyms = synonymsRaw.map((syn) => syn.offset);
  }

  let indexRaw = dict.index({
    include_offset: true,
    include_dictpos: false,
    include_term: false,
  });
  let index = indexRaw.map((idx) => idx.offset);

  let syn_obj;
  if (synonyms.length > 0) {
    let some_syn = (synonyms.length / 2) | 0;
    let syn_list = dict.synonyms({
      start_offset: synonyms[some_syn],
    });
    syn_obj = syn_list[0];
  } else {
    syn_obj = { term: null, wid: (index.length / 2) | 0 };
  }

  let idx = dict.index({
    start_offset: index[syn_obj.wid],
  });
  let idx_obj = idx[0];

  let entry = dict.entry(idx_obj.dictpos);
  let term_str = idx_obj.term;
  if (syn_obj.term !== null) {
    term_str = `${syn_obj.term} (${term_str})`;
  }
  let entry_str = entry
    .filter((data) => "mgtxykwhr".indexOf(data.type) != -1)
    .map((data) => `${data.type}: ${data.content}`)
    .join("; ");

  postMessage({ name, term_str, entry_str });
};
