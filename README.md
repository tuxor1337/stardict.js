# stardict.js

JavaScript module for handling dictionaries in StarDict format. The interface is rather low-level,
and does not implement higher-level convenience functions such as quick lookup of terms. The data is
accessed directly from the files (blobs) as needed, and not copied into memory unless otherwise
requested by the user.

stardict.js depends on [dictzip.js](https://framagit.org/tuxor1337/dictzip.js) to access dictzip
compressed files that might be part of the dictionary.

## Documentation

### Build and usage

Bundle this project as a single file in `./dist/stardict.js` as follows:

    $ npm install
    $ npm run build

Use an ES6 import statement to add the class `StarDict` to your scope:

    import { StarDict } from 'dist/stardict.js';

Create a new instance and initialize asynchronously by specifying an array of `File` (or `Blob`)
objects:

    let dict = new StarDict();
    await dict.load(files);

The `files` array is supposed to contain exactly one file of type \*.dict(.dz), \*.idx(.dz), \*.ifo
(and optionally more files listed in the StarDict format specification). Each instance of
`StarDict` provides the following methods:

    let value = dict.keyword(key);

This method is synchronous. `key` is a string and `value` is the value of the respective key in the
dictionary's \*.ifo file or `null` if key is not defined in the \*.ifo file. Format specific keys
include _version, bookname, wordcount, synwordcount, idxfilesize_ and _sametypesequence_. You will
typically be interested in the value of _bookname_.

A dictionary in the StarDict format comes with a word index and optionally synonyms with references
to this index. You access the index as follows:

    let aIdx = await dict.index(options);

Here `aIdx` is an array of objects, even if the dictionary contains only one or no entry. The form
of the objects contained in `aIdx` is determined by the optional `options` parameter:

    options = {
        startOffset: unsigned int, // default: 0
        count: unsigned int,       // default: 1 if startOffset != 0 else `wordcount`
        includeTerm: boolean,      // default: true
        includeDictpos: boolean,   // default: true
        includeOffset: boolean     // default: false
    };

All properties are optional. `startOffset` is the offset in the dictionary's \*.idx file from which
to start reading and `count` the (maximal) number of objects `aIdx` will contain. The `include\*`
properties determine the format of the objects in `aIdx`:

    aIdx[n] = {
        term: string,                // if includeTerm == true
        dictpos: [startbyte, size],  // if includeDictpos == true
        offset: unsigned int         // if includeOffset == true
    };

Here `dictpos` is of the form expected by the `entry` method (see below) and `offset` corresponds to
the `startOffset` property of `options`, thus represents the offset of this `term` in \*.idx.

In a perfectly analogous way you can access the dictionary's synonyms:

    let aSyns = await dict.synonyms(options);

Here `aSyns` is an array of objects, even if it contains only one or no entry. The form of the
objects contained in `aSyns` is determined by the optional `options` parameter:

    options = {
        startOffset: unsigned int, // default: 0
        count: unsigned int,       // default: 1 if startOffset != 0 else `wordcount`
        includeTerm: boolean,      // default: true
        includeWid: boolean,       // default: true
        includeOffset: boolean     // default: false
    };

All properties are optional. `startOffset` is the offset in the dictionary's \*.syn file from which
to start reading and `count` the (maximal) number of objects `aSyns` will contain. The `include\*`
properties determine the format of the objects in `aSyns`:

    aSyns[n] = {
        term: string,          // if includeTerm == true
        wid: unsigned int,     // if includeWid == true
        offset: unsigned int   // if includeOffset == true
    };

`offset` corresponds to the `startOffset` property of `options`, thus represents the offset of this
`term` in \*.syn. Note that `wid` is _not_ the value corresponding to the `startOffset` option of
the `index` method (cf. above). Instead it's the position of the corresponding object in the array
`dict.index()` of _all_ index terms.

You access the articles/entries of the dictionary (contained in \*.dict) via the method

    let aData = await dict.entry(dictpos);

`dictpos` is an object `{ offset, size }` as provided by the `index` method. Because each entry in
\*.dict is composed of several data blocks, `aData` is an array of data objects of the following
form:

    aData[n] = {
        type: string,                   // one of m,l,g,t,x,y,k,w,h,r,W,P,X
        content: ArrayBuffer or string
    };

The `type` of a data block is defined in the StarDict format documentation (see link below), e.g. m
is text/plain, h is text/html etc. `content` is a string for the data types m,g,t,x,y,k,w,h,r and
an ArrayBuffer else. It's up to the user to interpret the other types according to the StarDict
format documentation.

Finally, you can access the resources provided with the dictionary (and usually referenced in the
entries) using

    let file = await dict.resource(name);

`file` is a `File` object (or `undefined` if the resource doesn't exist) and `name` is a string.

Finally, for fast lookup of terms within the dictionary there is a method to get the offsets of all
entries in the index to be used with the `startOffset` parameter of the `StarDict.index` method:

    let buffer = await dict.oft(mode);

An `ArrayBuffer` is returned that is a concatenation of all offsets as 32-bit integers in
network byte order (big endian). The optional `mode` parameter can be one of `"index"` or
`"synonyms"` and specifies whether to return the offsets of the main or the synonym index.

### Synchronous interface

Furthermore, the project provides an (untested) synchronous API for use inside of web workers.
Import it into your worker's scope and use it as follows:

    import { StarDictSync } from 'dist/stardict.js';
    let dict = new StarDictSync(files);

The `load` method from the synchronous case is obsolete since the respective checks are done in the
constructor.

### StarDict word list sorting

This module also provides a JavaScript implementation of the `stardict_strcmp` function mentioned in
the official StarDict format specifications:

    import { starDictStrCmp } from 'dist/stardict.js';
    starDictStrCmp("apple", "pEar");

All world lists (e.g. in idx and syn files) are sorted according to this function. Having an
implementation of this function allows to look up words in those lists very fast using the binary
search algorithm.

### Examples

To run the examples in the `./demo/` directory, you first need to run `npm run build`. After that,
use `npm run demo:async` and `npm run demo:sync` to run the example using the asynchronous or
synchronous API, respectively (a browser window should open automatically).

## Further reading

- Format documentation: http://stardict-4.sourceforge.net/StarDictFileFormat
- Python lib: http://code.google.com/p/pytoolkits/source/browse/trunk/utils/stardict/StarDict.py
- Java lib: http://code.google.com/p/toolkits/source/browse/trunk/android/YAStarDict/src/com/googlecode/toolkits/stardict/StarDict.java
