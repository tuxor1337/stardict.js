import nodeResolve from "@rollup/plugin-node-resolve";
import commonjs from "@rollup/plugin-commonjs";

const plugins = [nodeResolve(), commonjs()];

export default [
  {
    input: "src/index.js",
    output: [{ file: "dist/stardict.js", format: "esm" }],
    plugins: plugins,
  },
];
