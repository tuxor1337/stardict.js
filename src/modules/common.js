/**
 * Copyright (c) 2024 Thomas Vogt
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

export const DEFAULT_PAD = 25;

export const starDictStrCmp = (() => {
  const CHARCODE_A_CAPITAL = "A".charCodeAt(0),
    CHARCODE_Z_CAPITAL = "Z".charCodeAt(0),
    CHARCODE_A_SMALL = "a".charCodeAt(0);

  const getUtf8Bytes = (() => {
    let encoder = new TextEncoder("utf-8");
    return (str) => encoder.encode(str);
  })();

  function isAsciiUpper(c) {
    return c >= CHARCODE_A_CAPITAL && c <= CHARCODE_Z_CAPITAL;
  }

  function asciiLower(c) {
    return isAsciiUpper(c) ? CHARCODE_A_SMALL + c - CHARCODE_A_CAPITAL : c;
  }

  function asciiStrCaseCmp(s1, s2) {
    let commonLen = Math.min(s1.length, s2.length);
    for (let i = 0; i < commonLen; i++) {
      let c1 = asciiLower(s1[i]),
        c2 = asciiLower(s2[i]);
      if (c1 != c2) return c1 - c2;
    }
    return s1.length - s2.length;
  }

  function strCmp(s1, s2) {
    let commonLen = Math.min(s1.length, s2.length);
    for (let i = 0; i < commonLen; i++) {
      if (s1[i] != s2[i]) return s1[i] - s2[i];
    }
    return s1.length - s2.length;
  }

  return (s1, s2) => {
    s1 = getUtf8Bytes(s1);
    s2 = getUtf8Bytes(s2);
    let cmp = asciiStrCaseCmp(s1, s2);
    return cmp == 0 ? strCmp(s1, s2) : cmp;
  };
})();

export function getUintAt(arr, offs, bigint) {
  // read 32 bit int in network byte order (big endian)
  // if bigint is true, read a 64 bit "big" int
  if (offs < 0) offs = arr.length + offs;
  let nbytes = !!bigint ? 8 : 4;
  let out = 0;
  for (let j = offs; j < offs + nbytes; j++) {
    out <<= 8;
    out |= arr[j] & 0xff;
  }
  return out;
}

export const readUtf8String = (() => {
  let decoder = new TextDecoder("utf-8");
  return (bytes) => decoder.decode(bytes);
})();

class IdxIter {
  constructor(mode, buffer, offsetbits) {
    this.mode = mode;
    this.view = new Uint8Array(buffer);
    this.offsetbits = offsetbits || "32";
    this.refLen = mode === "synonyms" ? 4 : this.offsetbits == "64" ? 12 : 8;
    this.offset = 0;
    this._clear();
  }

  _clear() {
    this._term = undefined;
    this._ref = undefined;
    this._eos = -1;
    for (let i = this.offset; i + this.refLen < this.view.length; i++) {
      if (this.view[i] == 0) {
        this._eos = i;
        break;
      }
    }
    this.nextOffset = this._eos < 0 ? undefined : this._eos + this.refLen + 1;
  }

  _read() {
    if (typeof this._term !== "undefined") return;
    if (this._eos < 0) return;
    this._term = readUtf8String(this.view.subarray(this.offset, this._eos));
    if (this.mode === "synonyms") {
      this._ref = getUintAt(this.view, this._eos + 1);
    } else {
      let bigint = this.offsetbits == "64";
      this._ref = {
        offset: getUintAt(this.view, this._eos + 1, bigint),
        size: getUintAt(this.view, this._eos + 1 + (bigint ? 8 : 4)),
      };
    }
  }

  ref() {
    this._read();
    return this._ref;
  }

  term() {
    this._read();
    return this._term;
  }

  next() {
    this.offset = this.nextOffset;
    this._clear();
  }

  isValid() {
    return !!this.nextOffset && this.nextOffset <= this.view.length;
  }
}

export function* IdxIterator(mode, buffer, offsetbits) {
  let itItem = new IdxIter(mode, buffer, offsetbits);
  while (itItem.isValid()) {
    yield itItem;
    itItem.next();
  }
}

export class StarDictBase {
  constructor() {
    this.files = {};
    this._keywords = {
      version: "",
      bookname: "",
      wordcount: "",
      synwordcount: "",
      idxfilesize: "",
      sametypesequence: "",
    };
    this._rifo = {
      version: "",
      filecount: "",
      ridxfilesize: "",
      idxoffsetbits: "32",
    };
  }

  keyword(key) {
    if (arguments.length == 0) return { ...this._keywords };
    return this._keywords[key];
  }

  _checkFiles(flist) {
    ["idx", "syn", "dict", "ifo", "rifo", "ridx", "rdic"].forEach((d) => {
      this.files[d] = null;
      for (let i = 0; i < flist.length; i++) {
        let fname = flist[i].name;
        if (fname.endsWith(`.${d}`) || fname.endsWith(`.${d}.dz`)) {
          this.files[d] = flist[i];
          flist.splice(i, 1);
        }
      }
    });

    // all remaining files are interpreted to be resource files
    this.files["res"] = flist;

    if (!this.files["dict"]) throw new Error("Missing *.dict(.dz) file!");
    if (!this.files["idx"]) throw new Error("Missing *.idx(.dz) file!");
    if (!this.files["ifo"]) throw new Error("Missing *.ifo(.dz) file!");
  }

  _processIfo(text) {
    let lines = text.trim().split("\n");
    if (lines.shift() != "StarDict's dict ifo file")
      throw new Error("Not a proper ifo file");
    lines.forEach((l) => {
      let w = l.split("=");
      // Handle missing *.syn file
      if (w[0] == "synwordcount" && !this.files["syn"]) w[1] = 0;
      this._keywords[w[0]] = w[1];
    });
  }

  _processRifo(text) {
    let lines = text.trim().split("\n");
    if (lines.shift() != "StarDict's storage ifo file")
      throw new Error("Not a proper rifo file");
    lines.forEach((l) => {
      let w = l.split("=");
      this._rifo[w[0]] = w[1];
    });
  }

  _findResfile(name) {
    return this.files["res"].find((f) => f.name == name);
  }

  _findResidx(buffer, name) {
    for (let itItem of IdxIterator(
      "index",
      buffer,
      this._rifo["idxoffsetbits"],
    )) {
      if (itItem.term() == name) {
        return itItem.ref();
      }
    }
    return { offset: -1, size: -1 };
  }

  _processEntryData(buffer) {
    let rawdata = new Uint8Array(buffer),
      arr = [],
      isSts = false,
      typeStr = "";
    if ("" != this._keywords["sametypesequence"]) {
      typeStr = this._keywords["sametypesequence"];
      isSts = true;
    }
    while (true) {
      let t, d;
      if (isSts) {
        t = typeStr[0];
        typeStr = typeStr.substr(1);
      } else {
        t = String.fromCharCode(rawdata[0]);
        rawdata = rawdata.subarray(1);
      }
      if (isSts && "" == typeStr) d = rawdata;
      else if (t == t.toUpperCase()) {
        let end = getUIntAt(rawdata, 0);
        d = rawdata.subarray(4, end + 4);
        rawdata = rawdata.subarray(end + 4);
      } else {
        let end = 0;
        while (rawdata[end] != 0) end++;
        d = rawdata.subarray(0, end);
        rawdata = rawdata.subarray(end + 1);
      }
      if ("mgtxykwh".indexOf(t) != -1) d = readUtf8String(d);
      else d = d.buffer;
      arr.push({ type: t, content: d });
      if (rawdata.length == 0 || (isSts && typeStr == "")) break;
    }
    return arr;
  }

  _readIndexObjs(mode, options, buffer, offset) {
    let arr = [];
    let nBytesRead = 0;
    for (let itItem of IdxIterator(mode, buffer)) {
      let obj = {};
      if (mode === "synonyms") {
        if (options["includeTerm"]) obj["term"] = itItem.term();
        if (options["includeWid"]) obj["wid"] = itItem.ref();
        if (options["includeOffset"]) obj["offset"] = offset + itItem.offset;
      } else {
        if (options["includeTerm"]) obj["term"] = itItem.term();
        if (options["includeDictpos"]) obj["dictpos"] = itItem.ref();
        if (options["includeOffset"]) obj["offset"] = offset + itItem.offset;
      }
      arr.push(obj);
      nBytesRead = itItem.nextOffset;
    }
    return { objs: arr, nBytesRead: nBytesRead };
  }

  _synonymsParseOpts(options) {
    if (typeof options === "undefined") options = {};

    const optionsDefault = {
      count: -1,
      startOffset: 0,
      includeTerm: true,
      includeWid: true,
      includeOffset: false,
    };

    for (let prop in optionsDefault) {
      if (typeof options[prop] === "undefined") {
        if (prop == "count" && typeof options["startOffset"] !== "undefined")
          options["count"] = 1;
        else options[prop] = optionsDefault[prop];
      }
    }

    if (options["count"] < 0)
      options["count"] = parseInt(this.keyword("synwordcount"));

    return options;
  }

  _indexParseOpts(options) {
    if (typeof options === "undefined") options = {};

    let optionsDefault = {
      count: -1,
      startOffset: 0,
      includeTerm: true,
      includeDictpos: true,
      includeOffset: false,
    };

    for (let prop in optionsDefault) {
      if (typeof options[prop] === "undefined") {
        if (prop == "count" && typeof options["startOffset"] !== "undefined")
          options["count"] = 1;
        else options[prop] = optionsDefault[prop];
      }
    }

    if (options["count"] < 0)
      options["count"] = parseInt(this.keyword("wordcount"));
    return options;
  }
}
