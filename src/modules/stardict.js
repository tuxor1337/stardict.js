/**
 * Copyright (c) 2024 Thomas Vogt
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import { DictZipFile } from "dictzip";

import {
  DEFAULT_PAD,
  readUtf8String,
  IdxIterator,
  StarDictBase,
} from "./common.js";

export async function readAsArrayBuffer(file, offset, size) {
  if (typeof offset === "undefined") offset = 0;
  if (typeof size === "undefined") size = -1;
  if (file.name.substr(-3) == ".dz") {
    let reader = new DictZipFile(file);
    await reader.load();
    return size >= 0 ? reader.read(offset, size) : reader.read(offset);
  } else {
    let reader = new FileReader();
    return new Promise((resolve, reject) => {
      reader.onload = (evt) => resolve(evt.target.result);
      reader.readAsArrayBuffer(
        size >= 0 ? file.slice(offset, offset + size) : file.slice(offset),
      );
    });
  }
}

async function readAsText(file, offset, size) {
  let buffer = await readAsArrayBuffer(file, offset, size);
  return readUtf8String(new Uint8Array(buffer));
}

class StarDict extends StarDictBase {
  async load(fileList) {
    this._checkFiles(fileList);
    this._processIfo(await readAsText(this.files["ifo"]));
    if (!!this.files["rifo"]) {
      this._processRifo(await readAsText(this.files["rifo"]));
    }
  }

  async resource(name) {
    name = name.replace(/^\x1E/, "").replace(/\x1F$/, "");
    let result = this._findResfile(name);
    if (typeof result === "undefined") {
      if (!this.files["ridx"] || !this.files["rdic"]) return undefined;
      let buffer = await readAsArrayBuffer(this.files["ridx"]);
      let { offset, size } = this._findResidx(buffer, name);
      if (offset < 0) return undefined;
      buffer = await readAsArrayBuffer(this.files["rdic"], offset, size);
      result = new File([buffer], name);
    }
    return result;
  }

  async entry(dictpos) {
    let { offset, size } = dictpos;
    let buffer = await readAsArrayBuffer(this.files["dict"], offset, size);
    return this._processEntryData(buffer);
  }

  async synonyms(options) {
    if (!this.files["syn"]) return [];
    options = this._synonymsParseOpts(options);
    return this._readIndexBuffered("synonyms", options);
  }

  async index(options) {
    options = this._indexParseOpts(options);
    return this._readIndexBuffered("index", options);
  }

  async oft(mode) {
    let f = (mode == "synonyms" ? "syn" : "idx") + ".oft";
    if (!!this.files[f]) return readAsArrayBuffer(this.files[f]);
    let count = parseInt(
      this.keyword((mode == "synonyms" ? "syn" : "") + "wordcount"),
    );
    const buffer = new ArrayBuffer(4 * count);
    const view = new DataView(buffer);
    let i = 0;
    for (let itItem of await this._iterIndex(mode)) {
      view.setUint32(4 * i++, itItem.offset, false);
    }
    return buffer;
  }

  async _readIndexBuffered(mode, options, offset, count, pad) {
    if (typeof offset === "undefined") offset = options["startOffset"];
    if (typeof count === "undefined") count = options["count"];
    if (typeof pad === "undefined") pad = DEFAULT_PAD;
    let file = this.files[mode == "synonyms" ? "syn" : "idx"];
    if (count <= 0 || file.size <= offset) return [];
    let size = count * pad;
    let buffer = await readAsArrayBuffer(file, offset, size);
    let { objs, nBytesRead } = this._readIndexObjs(
      mode,
      options,
      buffer,
      offset,
    );
    return objs.concat(
      await this._readIndexBuffered(
        mode,
        options,
        offset + nBytesRead,
        count - objs.length,
        pad + DEFAULT_PAD,
      ),
    );
  }

  async _iterIndex(mode) {
    let f = mode == "synonyms" ? "syn" : "idx";
    let buffer = [];
    if (!!this.files[f]) {
      buffer = await readAsArrayBuffer(this.files[f]);
    }
    return IdxIterator(mode, buffer);
  }
}

export default StarDict;
