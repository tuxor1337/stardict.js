/**
 * Copyright (c) 2024 Thomas Vogt
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import { starDictStrCmp } from "../index.js";

test("compare strings according to StarDict specs", () => {
  expect(starDictStrCmp("apple", "apple")).toBe(0);
  expect(starDictStrCmp("apple", "appleee")).toBe(-2);
  expect(starDictStrCmp("apple", "ApPleee")).toBe(-2);
  expect(starDictStrCmp("apple", "ApPle")).toBe(32);
  expect(starDictStrCmp("apple", "pEar")).toBe(-15);
  expect(starDictStrCmp("pear", "pineapple")).toBe(-4);
  expect(starDictStrCmp("pear", "pineäpple")).toBe(-4);
  expect(starDictStrCmp("pear", "peär")).toBe(-98);
  expect(starDictStrCmp("pear", "peärs")).toBe(-98);
  expect(starDictStrCmp("peärs", "peär")).toBe(1);
  expect(starDictStrCmp("ap-ple", "apple")).toBe(-67);
});
