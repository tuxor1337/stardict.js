/**
 * Copyright (c) 2024 Thomas Vogt
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

import fs from "fs";
import path from "path";
import mime from "mime-types";

import { StarDict, readAsArrayBuffer } from "../index.js";

function createFileFromPath(p) {
  const { mtimeMs: lastModified, size } = fs.statSync(p);
  return new File([new fs.readFileSync(p)], path.basename(p), {
    lastModified,
    type: mime.lookup(p) || "",
  });
}

function createFileFromString(name, str) {
  return new File([str], name);
}

function createFileFromBytes(name, bytes) {
  return new File([new Uint8Array(bytes)], name);
}

function stoi(str) {
  return Array.from(str)
    .map((c) => c.charCodeAt(0))
    .concat([0x00]);
}

function i32to8(ints) {
  const buffer = new ArrayBuffer(4 * ints.length);
  const view = new DataView(buffer);
  ints.forEach((val, i) => view.setUint32(4 * i, val, false));
  return Array.from(new Uint8Array(buffer));
}

function buftoi8(buffer) {
  return Array.from(new Uint8Array(buffer));
}

function buftoi32(buffer) {
  let arr = [];
  const view = new DataView(buffer);
  for (let i = 0; 4 * (i + 1) <= buffer.byteLength; i++) {
    arr.push(view.getUint32(4 * i, false));
  }
  return arr;
}

describe("read a dictionary in StarDict format", () => {
  let sd, idx;
  beforeEach(async () => {
    const files = ["dict.dz", "idx"].map((suf) =>
      createFileFromPath(`./src/tests/data/testdict.${suf}`),
    );
    files.push(
      createFileFromString(
        "testdict.ifo",
        "StarDict's dict ifo file\n" +
          "version=3.0.0\n" +
          "bookname=A foo-bar dictionary\n" +
          "wordcount=4\n" +
          "synwordcount=2\n" +
          "idxfilesize=60\n" +
          "sametypesequence=m\n" +
          "description=\n",
      ),
      createFileFromBytes(
        "testdict.syn",
        [stoi("abc"), i32to8([3]), stoi("synonym two"), i32to8([2])].flat(),
      ),
      createFileFromBytes("someimg.png", [0xa0, 0x02, 0x01]),
      createFileFromString(
        "res.rifo",
        "StarDict's storage ifo file\n" +
          "version=3.0.0\n" +
          "filecount=3\n" +
          "ridxfilesize=60\n" +
          "idxoffsetbits=64\n",
      ),
      createFileFromBytes(
        "res.ridx",
        [
          stoi("imagea.png"),
          i32to8([0, 0, 3]),
          stoi("imageb.png"),
          i32to8([0, 3, 5]),
          stoi("resourcec.dat"),
          i32to8([0, 8, 2]),
        ].flat(),
      ),
      createFileFromBytes(
        "res.rdic",
        [0xa0, 0x02, 0x01, 0xff, 0x01, 0x00, 0x04, 0x30, 0xf0, 0x20],
      ),
    );
    sd = new StarDict();
    await sd.load(files);
    idx = await sd.index();
  });

  it("uses valid test-helpers", () => {
    expect(i32to8([17, 4])).toStrictEqual([
      0x00, 0x00, 0x00, 0x11, 0x00, 0x00, 0x00, 0x04,
    ]);
  });

  it("parses the ifo-file correctly", async () => {
    expect(sd.keyword("bookname")).toBe("A foo-bar dictionary");
  });

  it("returns all ifo properties when called without arguments", async () => {
    expect(sd.keyword()).toStrictEqual({
      version: "3.0.0",
      bookname: "A foo-bar dictionary",
      wordcount: "4",
      synwordcount: "2",
      idxfilesize: "60",
      sametypesequence: "m",
      description: "",
    });
  });

  it("skips trailing new-line when parsing rifo", async () => {
    expect(sd._rifo).toStrictEqual({
      version: "3.0.0",
      filecount: "3",
      ridxfilesize: "60",
      idxoffsetbits: "64",
    });
  });

  it("parses the idx-file correctly", async () => {
    expect(idx).toStrictEqual([
      { term: "another", dictpos: { offset: 0, size: 8 } },
      { term: "foo", dictpos: { offset: 8, size: 3 } },
      { term: "lorem", dictpos: { offset: 11, size: 5 } },
      { term: "some word", dictpos: { offset: 16, size: 13 } },
    ]);
    let oft = buftoi32(await sd.oft());
    expect(oft).toStrictEqual([0, 16, 28, 42]);
    expect(await sd.index({ startOffset: 16 })).toStrictEqual([
      { term: "foo", dictpos: { offset: 8, size: 3 } },
    ]);
  });

  it("parses the syn-file correctly", async () => {
    let syn = await sd.synonyms();
    expect(syn).toStrictEqual([
      { term: "abc", wid: 3 },
      { term: "synonym two", wid: 2 },
    ]);
    let oft = buftoi32(await sd.oft("synonyms"));
    expect(oft).toStrictEqual([0, 8]);
  });

  it("extracts entry data", async () => {
    let entries = [];
    for (let i of idx) {
      entries.push(await sd.entry(i["dictpos"]));
    }
    expect(entries).toStrictEqual([
      [{ type: "m", content: "translat" }],
      [{ type: "m", content: "bar" }],
      [{ type: "m", content: "ipsum" }],
      [{ type: "m", content: "a translation" }],
    ]);
  });

  it("extracts resource data", async () => {
    let resName, res;

    resName = "not_exist.png";
    res = await sd.resource(resName);
    expect(res).toBe(undefined);

    resName = "someimg.png";
    res = await sd.resource(resName);
    expect(res.name).toBe(resName);
    expect(buftoi8(await readAsArrayBuffer(res))).toStrictEqual([
      0xa0, 0x02, 0x01,
    ]);

    resName = "imageb.png";
    res = await sd.resource(resName);
    expect(res.name).toBe(resName);
    expect(buftoi8(await readAsArrayBuffer(res))).toStrictEqual([
      0xff, 0x01, 0x00, 0x04, 0x30,
    ]);
  });
});
